<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 07.05.2019
 * Time: 22:20
 */

namespace App\Http\Controllers;

use App\DocxConversion;
use App\Templates;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TemplatesController {
	public static function templatesList() {
		$templates = Templates::getTemplates();
		return $templates;
	}

	public static function getDocFields($id) {
		$sql = "SELECT name FROM document_fields WHERE doc_id=:id";
		$data = DB::select(DB::raw($sql), ['id' => $id]);
		$fields = [];
		foreach ($data as $row) {
			$fields[] = $row->name;
		}
		return response()->json(['fields'=>$fields]);
	}

	public function storeTemplate(Request $request) {
		DB::table('templates')->insert(
			['filename' => $request->name]
		);
	}

	public static function deleteItem(Request $request) {
		$document = Templates::getDocument($request->id);
		$filename = $document[0]->filename;
		if (empty($filename)) { return false; }
		$removed = unlink(public_path('templates/'.$filename));
		if ($removed) {
			Templates::find($request->id)->delete();
		}
	}

	public function submit(Request $request) {
		$fields = request()->post();
		$document_id = $fields['documentId'];
		$document = Templates::getDocument($document_id);
		$filename = $document[0]->filename;
		if (empty($filename)) { return false; }
		$content = public_path('templates/'.$filename);
		$docObj = new DocxConversion($content);
		$docText = $docObj->convertToText();
		foreach ($fields as $field => $value) {
			$bracket_pattern  = '/(\$\[[^]]*?)(\b'.$field.'\b)([^]]*?\])/m';
			$c_braces_pattern = '/(\$\{[^}]*?)(\b'.$field.'\b)([^}]*?\})/m';
			$docText = preg_replace($bracket_pattern, $value, $docText);
			$docText = preg_replace($c_braces_pattern, $value, $docText);
		}
		$phpWord = new \PhpOffice\PhpWord\PhpWord();
		$section = $phpWord->addSection();
		$section->addText(
			$docText
		);
		$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		$new_file = time().'_'.$filename;
		$objWriter->save(public_path('generated/'.$new_file));
		return $new_file;
	}

	public static function download($filename) {
		if (empty($filename)) { return false; }
		$file_path = public_path('generated/'.$filename);
		return response()->download($file_path);
	}

	public function uploadTemplate(Request $request) {
		$request->validate([
			'file' => 'required|file|mimes:docx,doc',
		]);
		$file = time().'.'.$request->file->getClientOriginalExtension();
		$uploaded = $request->file->move(public_path('templates'), $file);
		if (!$uploaded) { return false; }
		$doc_id = DB::table('templates')->insertGetId(['filename' => $file]);
		$fields = [];
		if ($doc_id) {
			$content = public_path('templates/'.$file);
			$docObj = new DocxConversion($content);
			$docText = $docObj->convertToText();
			$bracket_pattern = '/\$\[+(.*?)]/';
			$c_braces_pattern = '/\$\{+(.*?)}/';
			preg_match_all($bracket_pattern, $docText, $brackets);
			preg_match_all($c_braces_pattern, $docText, $c_braces);
			foreach ($brackets[1] as $bracket_field) {
				if (!in_array($bracket_field, $fields)) {
					$fields[] = $bracket_field;
				}
			}
			foreach ($c_braces[1] as $braces_field) {
				if (!in_array($braces_field, $fields)) {
					$fields[] = $braces_field;
				}
			}
			$inserted_data = [];
			foreach ($fields as $fieldname) {
				$inserted_data[] = ['doc_id' => $doc_id, 'name' => $fieldname];
			}
			DB::table('document_fields')->insert($inserted_data);
		}

		return response()->json([
			'success' => 'You have successfully uploaded template.',
			'fields' => $fields
		]);
	}
}