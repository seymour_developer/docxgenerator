require('./bootstrap');

window.Vue = require('vue');

import VueModalTor from 'vue-modaltor'
Vue.use(VueModalTor);

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('example-modal', require('./components/NewModal.vue').default);
Vue.component('example-component', require('./components/ImageUploadComponent.vue').default);
//Vue.component('modal', require('./components/Modal.vue').default);

//Vue.component('example-modal', {template: '#example-modal'});
//Vue.component('example-modal', {template: '#example-modal'});


new Vue({
	el: '#app',
	/* data: {
		open: false
	},

    methods: {
		hideModal() {
			this.open = false
		},
		showModal() {
			let element = this.$refs.modal.$el
			//console.log(element);
			$(element).modal('show')
		}
	}*/
});

/*const list = new Vue({
	el: '#vue-wrapper',

	data: {
		items: [],
		hasError: true,
		hasDeleted: true,
		newItem : {'name':''}
	},
	mounted : function(){
		this.getVueItems();
	},
	methods : {
		getVueItems: function(){
			axios.get('/vueitems').then(response => {
				this.items = response.data;
            });
		},
		createItem: function(){
			var input = this.newItem;
			if(input['name'] == ''){
				this.hasError = false;
				this.hasDeleted = true;
			} else {
				this.hasError = true;
				axios.post('/vueitems', input)
					.then(response => {
						this.newItem = {'name':''};
						this.getVueItems();
					});
				this.hasDeleted = true;
			}
		},
		deleteItem: function(item){
			axios.post('/vueitems/'+item.id).then((response) => {
				this.getVueItems();
				this.hasError = true,
                    this.hasDeleted = false
			});
		},
	}
});*/


